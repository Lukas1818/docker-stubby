FROM alpine
SHELL ["/bin/ash", "-uo", "pipefail", "-c"]

ARG GETDNS_VERSION=1.7.2
ARG STUBBY_VERSION=0.4.2

LABEL org.opencontainers.image.url="https://gitlab.com/LuckyTurtleDev/docker-stubby/container_registry"
LABEL org.opencontainers.image.title="Stubby on Alpine Linux"
LABEL org.opencontainers.image.source="https://gitlab.com/LuckyTurtleDev/docker-stubby"
LABEL org.opencontainers.image.version="${STUBBY_VERSION}"

RUN apk add --no-cache dnssec-root libressl unbound yaml \
 # build getdns
 && apk add --no-cache --virtual .build-deps binutils cmake gcc openssl-dev musl-dev ninja unbound-dev yaml-dev \
 && wget -qO- "https://getdnsapi.net/releases/getdns-${GETDNS_VERSION//./-}/getdns-${GETDNS_VERSION}.tar.gz" | tar xfz - \
 && sed -i -e '/HAVE_SYS_POLL_H/d' "getdns-${GETDNS_VERSION}/cmake/include/cmakeconfig.h.in" \
 && mkdir build \
 && cmake "getdns-${GETDNS_VERSION}" \
        -B build \
		-G Ninja \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DCMAKE_INSTALL_SYSCONFDIR=/etc \
        -DCMAKE_BUILD_TYPE=MinSizeRel \
        -DENABLE_STATIC=OFF \
        -DBUILD_TESTING=OFF \
        -DBUILD_GETDNS_QUERY=OFF \
        -DBUILD_GETDNS_SERVER_MON=OFF \
        -DBUILD_LIBEV=OFF \
        -DBUILD_LIBEVENT2=OFF \
        -DBUILD_LIBUV=OFF \
        -DUSE_LIBIDN2=OFF \
        -DPATH_TRUST_ANCHOR_FILE=/usr/share/dnssec-root/trusted-key.key \
 && ninja -C build install \
 && rm -rf build "getdns-${GETDNS_VERSION}" \
 # build stubby
 && wget -qO- "https://github.com/getdnsapi/stubby/archive/refs/tags/v${STUBBY_VERSION}.tar.gz" | tar xfz - \
 && mkdir build \
 && cmake "stubby-${STUBBY_VERSION}" \
        -B build \
		-G Ninja \
        -DCMAKE_INSTALL_PREFIX=/usr/local \
        -DCMAKE_INSTALL_SYSCONFDIR=/etc \
        -DCMAKE_BUILD_TYPE=MinSizeRel \
 && ninja -C build install \
 && strip /usr/local/bin/stubby \
 && rm -rf build "stubby-${STUBBY_VERSION}" \
 && apk del --no-cache .build-deps \
 && for dir in doc include man; do rm -rf /usr/local/share/$dir; done

COPY stubby.yml /etc/stubby/

EXPOSE 53/UDP

CMD ["/usr/local/bin/stubby", "-l"]
